const express = require('express')
const app = express()
const port = 8000

app.get('/', (req, res) => {
    res.send('This is a Sample for CICD Pipeline')
})

app.listen(port, () => {
    console.log('Application is listening at http://localhost:${port}')
})